﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;
using System.Windows.Forms;

namespace SiteDeployer
{
     public static  class IISSiteInstaller

    {
        public static bool isSuccess;
        static string siteName;
        static string chosenDirectory;
        static string zipfileName;
        static string unzippedfolderName;
        static Uri projectURL;
        static WebClient webClient;
        static ServerManager serverManager;


        public async static Task IISInstall(string linkURL,string chosenDir,string sName)
        {
            webClient = new WebClient();
            try
            {
                projectURL = new Uri(linkURL);

            }
            catch(Exception exc)
            {
                string message = exc.Message;
                string caption = "Ошибка при попытке распознать ссылку";
                MessageBox.Show(message,caption, MessageBoxButtons.OK);
                return ;              
            }

            chosenDirectory = chosenDir;
            siteName = sName;

            try
            {
              await DownLoad();
            }
            catch(Exception exc)
            {
                string message = exc.Message + Environment.NewLine + "Возможные причины:" +
                    Environment.NewLine + "неверная ссылка;" +
                    Environment.NewLine + "нет ответа от сервера (можно попробовать еще раз)";
                string caption = "Не удалось загрузить проект.";
                MessageBox.Show(message,caption,MessageBoxButtons.OK);
                return ;
            }

            try
            {
                Unzip();
            }
            catch ( Exception exc )
            {
                string message = exc.Message;

                string caption = "Не удалось извлечь проект из архива.";

                MessageBox.Show(message,caption,MessageBoxButtons.OK);
                return ;
            }

            try
            {
                AddNewSite();
            }
            catch ( Exception exc )
            {
                string message = exc.Message;

                string caption = "Не удалось добавить новый сайт";

                MessageBox.Show(message,caption,MessageBoxButtons.OK);
                return ;
            }

            isSuccess =  CheckIfSiteAdded();


        }
        static void AddNewSite()
        {
            serverManager = new ServerManager();
            Site myNewSite = ( (SiteCollection)( serverManager.Sites ) ).CreateElement();
            myNewSite.ServerAutoStart = true;
            myNewSite.Id = 1234567890;
            myNewSite.SetAttributeValue("Name",siteName);
                       
            ((SiteCollection)( serverManager.Sites ) ).Add(myNewSite);

            string poolName = "UnsecuredLocalSystem";
            serverManager.ApplicationPools.Add(poolName);
            ApplicationPool appPool = serverManager.ApplicationPools [poolName];
            appPool.ProcessModel.IdentityType = ProcessModelIdentityType.LocalSystem;
            appPool.ManagedRuntimeVersion = "v4.0";
            appPool.ManagedPipelineMode = ManagedPipelineMode.Integrated;

            Microsoft.Web.Administration.Application app = myNewSite.Applications.CreateElement();
            app.SetAttributeValue("path","/");
            app.SetAttributeValue("applicationPool",poolName);
          
            myNewSite.Applications.Add(app);

            VirtualDirectory vdir = app.VirtualDirectories.CreateElement();
            vdir.SetAttributeValue("path","/");
            vdir.SetAttributeValue("physicalPath",chosenDirectory + "\\" + unzippedfolderName);
            app.VirtualDirectories.Add(vdir);

            Microsoft.Web.Administration.Binding b = myNewSite.Bindings.CreateElement();
            b.SetAttributeValue("protocol","http");
            b.SetAttributeValue("bindingInformation",":54321:" + "localhost");
            myNewSite.Bindings.Add(b);
          
            serverManager.CommitChanges();

        }

        async static Task DownLoad()
        {
            zipfileName = projectURL.LocalPath.Replace('/','-').Replace("get","").Remove(0,1);

         await webClient.DownloadFileTaskAsync(projectURL, chosenDirectory + "\\" + zipfileName);
           
           
        }
        static void Unzip()
        {
            ZipFile.ExtractToDirectory(chosenDirectory + "\\" + zipfileName, chosenDirectory);

            using ( ZipArchive archive = ZipFile.OpenRead(chosenDirectory + "\\" + zipfileName) )
            {
                ZipArchiveEntry entry = archive.Entries.FirstOrDefault();
                unzippedfolderName = entry.FullName.TrimEnd('/');
            }

        }
        static bool CheckIfSiteAdded()
        {
            foreach ( var site in serverManager.Sites)
            {
                if ( site.Name == siteName )
                {
                    return true;
                }
            }
            return false;
            

        }

    }
}
