﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SiteDeployer
{
    public partial class MainForm : Form
    {
        public static string siteName;
        public MainForm()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender,EventArgs e)
        {
           SiteNameChooser();
           
            this.folderBrowserDialog1.Description = "Выберите или создайте папку, для физического хранения файлов сайта. Не указывайте папку на рабочем столе.";
            if ( this.folderBrowserDialog1.ShowDialog() == DialogResult.OK )
            {
              await IISSiteInstaller.IISInstall(this.textBoxURL.Text,this.folderBrowserDialog1.SelectedPath,siteName);

                if (! IISSiteInstaller.isSuccess )
                {
                    MessageBox.Show("Не удалось развернуть сайт","Ошибка",MessageBoxButtons.OK);
                    return;
                }
                else
                {
                    MessageBox.Show("Сайт развернут на localhost","Успех",MessageBoxButtons.OK);
                    System.Diagnostics.Process.Start("http://localhost:54321/");
                }
            }
        }

        private void SiteNameChooser()
        {
            NameSiteForm nameForm = new NameSiteForm();
            nameForm.ShowDialog();
           
        }
    }
}
