﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SiteDeployer
{
    public partial class NameSiteForm : Form
    {
       
        public NameSiteForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender,EventArgs e)
        {
            if ( this.textBox1.Text.Length < 1 )
            {
                MessageBox.Show("Придумайте имя сайта","У сайта должно быть имя",MessageBoxButtons.OK);
                return;
            }
            else
            {
               MainForm.siteName = this.textBox1.Text;
                this.Close();
            }
        }
    }
}
